import React, { useEffect, useState } from 'react'

const Count = () => {
    const [count, setCount] = useState(0);
    const increaseCount = () => {
        console.log("increase count");
        setCount(count + 1);
    }

    useEffect(() => {
        console.log("componentDidUpdate!");
        document.title = `You clicked ${count} times!'`
        return () => {
            console.log("componentWillUnmount")
        }
    })

    useEffect(() => {
        console.log("componentDidMount!");
        document.title = `You clicked ${count} times!'`
    }, [])

    return (
        <div>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCount}>Click me!</button>
        </div>
    )
}

export default Count
